# frozen_string_literal: true

require 'rpi_gpio'

CPU_TEMP_FILENAME = '/sys/class/thermal/thermal_zone0/temp'

module Pifan
  class Process
    include Pifan::Logging

    def initialize(parameters)
      @control_pin = parameters[:control_pin]
      @refresh_time = parameters[:refresh_time]
      @pwm_frequency = parameters[:pwm_frequency]
      @temp_steps = parameters[:temperature_steps]
      @speed_steps = parameters[:speed_steps]

      RPi::GPIO.set_numbering :bcm
      RPi::GPIO.setup @control_pin, as: :output, initialize: :low
      @fan = RPi::GPIO::PWM.new(@control_pin, @pwm_frequency)
      @fan.start(0)
    end

    def start
      hyst = 1
      cpu_temp_old = 0
      fan_speed_old = 0

      # Run at full speed for 2 secs to avoid not starting at small speed
      @fan.duty_cycle = 100
      sleep 2

      loop do
        cpu_temp_current = cpu_temp
        logger.debug("New temperature: #{cpu_temp_current}")

        if (cpu_temp_current - cpu_temp_old).abs > hyst
          fan_speed_current = fan_speed
          if fan_speed_current != fan_speed_old
            logger.debug("New fan speed: #{fan_speed_current}")
            @fan.duty_cycle = fan_speed_current
            fan_speed_old = fan_speed_current
          end

          cpu_temp_old = cpu_temp_current
        end

        sleep @refresh_time
      end
    end

    def cleanup
      RPi::GPIO.clean_up
    end

    private

    def cpu_temp
      File.read(CPU_TEMP_FILENAME).to_f / 1000
    end

    def fan_speed
      if cpu_temp <= @temp_steps.first
        @speed_steps.first
      elsif cpu_temp >= @temp_steps.last
        @speed_steps.last
      else
        more_index = @temp_steps.find_index { |n| cpu_temp < n }
        less_index = more_index - 1

        speed_interpolation less_index, more_index
      end
    end

    def speed_interpolation(less_index, more_index)
      ((@speed_steps[more_index] - @speed_steps[less_index]) / \
        (@temp_steps[more_index] - @temp_steps[less_index]) * \
        (cpu_temp - @temp_steps[less_index]) + \
        @speed_steps[less_index]).round 1
    end
  end
end
