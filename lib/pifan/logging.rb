# frozen_string_literal: true

require 'tty-logger'

module Pifan
  module Logging
    class << self
      def level
        @level ||= :error
      end

      def logger
        @logger ||= TTY::Logger.new do |config|
          config.level = level
        end
      end

      attr_writer :logger, :level
    end

    def self.included(base)
      class << base
        def logger
          Logging.logger
        end
      end
    end

    def logger
      Logging.logger
    end
  end
end
