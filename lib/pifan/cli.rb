# frozen_string_literal: true

require 'tty-option'

module Pifan
  class CLI
    include TTY::Option

    usage do
      no_command

      desc 'Control your Raspberry Pi fan'
    end

    flag :help do
      short '-h'
      long '--help'
      desc 'Print usage'
    end

    flag :version do
      short '-v'
      long '--version'
      desc 'Print version'
    end

    flag :debug do
      short '-d'
      long '--debug'
      desc 'Run in debug mode'
      default false
    end

    option :control_pin do
      short '-p'
      long '--control-pin pin'
      desc 'Pin to control Raspberry Pi fan with'
      optional
      convert :int
    end

    option :refresh_time do
      short '-t'
      long '--refresh-time time'
      desc 'Time to wait between each refresh in seconds'
      optional
      convert :int
    end

    option :pwm_frequency do
      short '-f'
      long '--pwm-frequency frequency'
      desc 'PWM frequency in Hz'
      optional
      convert :int
    end

    option :temperature_steps do
      long '--temperature-steps steps'
      desc 'Temperature steps in °C'
      optional
      convert :ints
    end

    option :speed_steps do
      long '--speed-steps steps'
      desc 'Speed steps in percents'
      optional
      convert :ints
    end

    def run
      if params[:help]
        print help
        exit
      elsif params[:version]
        $stdout.puts Pifan::VERSION
        exit
      else
        params.to_h
      end
    end
  end
end
