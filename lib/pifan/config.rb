# frozen_string_literal: true

require 'tty-config'

module Pifan
  class Config
    def initialize
      @config = TTY::Config.new
      @config.filename = 'pifan'
      @config.extname = '.yml'
      @config.append_path('/etc/pifan')
      @config.append_path("#{File.dirname(__FILE__)}/../../config")
    end

    def read
      @config.read.deep_transform_keys { |key| key.underscore.to_sym }
    end
  end
end
