# frozen_string_literal: true

require 'active_support/all'

module Pifan
  autoload :Logging, 'pifan/logging'
  autoload :CLI, 'pifan/cli'
  autoload :Config, 'pifan/config'
  autoload :Process, 'pifan/process'
end

require 'pifan/version'
