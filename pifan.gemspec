# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pifan/version'

Gem::Specification.new do |spec|
  spec.name          = 'pifan'
  spec.version       = Pifan::VERSION
  spec.authors       = ['Michal Kimle']
  spec.email         = ['kimle.michal@gmail.com']

  spec.summary       = 'Control your Raspberry Pi fan'
  spec.description   = "Pifan is a daemon adjusting your fan's strength based on your Raspberry Pi's temperature."
  spec.homepage      = 'https://gitlab.com/amarthadan/pifan'
  spec.license       = 'GPL-3.0-only'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'pry', '~> 0.13'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rubocop', '~> 0.92'

  spec.add_runtime_dependency 'activesupport', '~> 6.0'
  spec.add_runtime_dependency 'rpi_gpio', '~> 0.5'
  spec.add_runtime_dependency 'tty-config', '~> 0.4'
  spec.add_runtime_dependency 'tty-logger', '~> 0.5'
  spec.add_runtime_dependency 'tty-option', '~> 0.1'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')
end
