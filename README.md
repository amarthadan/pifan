# Pifan
**Control your Raspberry Pi fan**

Pifan is a daemon adjusting your fan's strength based on your Raspberry Pi's temperature.

The whole project was inspired and based on this [instructable](https://www.instructables.com/PWM-Regulated-Fan-Based-on-CPU-Temperature-for-Ras/). Many thanks to the author [Aerandir14](https://www.instructables.com/member/Aerandir14/)!
## Hardware configuration
The most basic configuration is depicted on the following schematics:

<img src="schematics/pifan-breadboard.png" width="800"/>
<br/>
<img src="schematics/pifan-schematic.png" width="800"/>

This is the most common setup with **5V** fan rated for **200mA**. If you have different components, read through the [instructable](https://www.instructables.com/PWM-Regulated-Fan-Based-on-CPU-Temperature-for-Ras/), you can find instructions on how to calculate the correct values for your setup.

## Installation
### Arch Linux
You can install the [`pifan`](https://aur.archlinux.org/packages/pifan) package from AUR.
```bash
yay -S pifan
```

### RubyGems.org
To install the most recent stable version:
```bash
gem install pifan
```

### Source (development)
**Installation from source should never be your first choice! Especially, if you are not familiar with RVM, Bundler, Rake and other dev tools for Ruby!**

**However, if you wish to contribute to my project, this is the right way to start.**

To build and install the bleeding edge version from master:
```bash
git clone git://gitlab.com/amarthadan/pifan.git
cd pifan
gem install bundler
bundle install
```

## Configuration
Configuration is read from `/etc/pifan/pifan.yml` (file is automatically created when installed [via package method](#arch-linux)). Sample (and also the default) configuration file can be found at [`config/pifan.yml`](config/pifan.yml).

Pifan can be also controlled via CLI options. Run
```bash
pifan -h
```
to list all the available options.

## Usage
You can run Pifan manually via `pifan` command. This is particularly useful in order to figure out the correct configuration for your setup. Once you know the correct values, you can use [`systemd/pifan.service`](systemd/pifan.service) systemd unit to control Pifan as a daemon (service unit is automatically available when installed [via package method](#arch-linux)).

```bash
sudo systemctl enable pifan
sudo systemctl start pifan
```

## Development & contribution
Contributions are very welcome! To setup the project for development follow the [installation from source](#source-development) instructions.

Contribution flow:
1. Fork it (https://gitlab.com/amarthadan/pifan/fork)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request
